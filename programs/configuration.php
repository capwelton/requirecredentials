<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
require_once dirname(__FILE__).'/functions.php';
require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';

class requirecredentials_configEditor
{
	private function getForm()
	{
		$W = bab_functionality::get('Widgets');
		/*@var $W Func_Widgets */
		
		$form = $W->Form()->setLayout($W->VBoxItems()->setVerticalSpacing(2, 'em'));
		$form->setName('configuration');
		$form->setHiddenValue('tg', bab_rp('tg'));
		
		$form->addItem(
		    $W->LabelledWidget(
		        requirecredentials_translate('Require credentials for all pages'),
		        $W->CheckBox(),
		        'enabled'
	        )
	    );
		
		$form->addItem(
			$W->LabelledWidget(
			    requirecredentials_translate('List of exceptions (one url per line)'), 
			    $W->TextEdit()->addClass('widget-full-width'),
			    'exceptions',
			    requirecredentials_translate('Do not redirect to the login form if the url contain one of the lines')
            )
		);
		
		
		
		$form->addItem(
		    $W->LabelledWidget(
		        requirecredentials_translate('Propose the authentication methods before authenticate with the default method'),
		        $choose = $W->CheckBox(),
		        'choose'
		    )
		);
		
		$form->addItem($dialogOptions = $W->Frame(null, $W->VBoxItems()->setVerticalSpacing(2, 'em')));
		
		
		$dialogOptions->addItem(
		    $W->LabelledWidget(
		        requirecredentials_translate('Fallback to default authentication method method after'),
		        $W->LineEdit()->setSize(3),
		        'timeout',
		        null,
		        requirecredentials_translate('seconds')
	        )
	    );
		
		
		
		$dialogOptions->addItem(
		    $W->LabelledWidget(
		        requirecredentials_translate('Modal dialog'),
		        $W->CheckBox(),
		        'modal'
	        )
	    );
		
		
		$dialogOptions->addItem(
		    $W->LabelledWidget(
		        requirecredentials_translate('Content to display before the authenticate with default method button'),
		        $W->SimpleHtmlEdit()->addClass('widget-fullwidth'),
		        'tophtml'
	        )
	    );
		
		
		$dialogOptions->addItem(
		    $W->LabelledWidget(
		        requirecredentials_translate('Text to display before the alternative methods'),
		        $W->TextEdit()->addClass('widget-fullwidth'),
		        'bottomtext'
	        )
	    );
		
		
		$choose->setAssociatedDisplayable($dialogOptions, array(1));
		
		
		

		
		$form->addItem(
			$W->SubmitButton()
				->setLabel(requirecredentials_translate('Save'))
		);
		
		$registry = bab_getRegistryInstance();
		$registry->changeDirectory('/requirecredentials/');
		
	    $default = array(
	        'tg=login&cmd=authform',
	        'tg=login&cmd=emailpwd',
	        'tg=login&cmd=register',
	        'captchaIdx&sIdx=getImage',
	        'tg=login&cmd=confirm'
	    );
		
	    $enabled = $registry->getValue('enabled', true);
		$exceptions = $registry->getValue('exceptions', implode("\n", $default));
		$choose = $registry->getValue('choose', true);
		$timeout = $registry->getValue('timeout', 3);
		$modal = $registry->getValue('modal', false);
		$tophtml = $registry->getValue('tophtml', '');
		$bottomtext = $registry->getValue('bottomtext', '');
		
		
		$form->setValues(
		    array(
		        'configuration' => array(
		            'enabled' => $enabled,
        		    'exceptions' => $exceptions,
        		    'choose' => $choose,
        		    'timeout' => $timeout,
		            'modal' => $modal,
		            'tophtml' => $tophtml,
		            'bottomtext' => $bottomtext
        		)
		    )
		);
		
		return $form;
	}
	
	
	
	public function display()
	{
		$W = bab_functionality::get('Widgets');
		/*@var $W Func_Widgets */
		$page = $W->BabPage();

		$page->setTitle(requirecredentials_translate('Require credentials options'));
		
		
		$frame = $W->Frame();
		$frame->setLayout($W->VBoxLayout()->setVerticalSpacing(2, 'em'));
		
		$frame->addClass('BabLoginMenuBackground');
		$frame->addClass('widget-bordered');
		

		$configurationForm = $this->getForm();
		
		$frame->addItem($configurationForm);

		$page->addItem($frame);
		
		$page->displayHtml();
	}
	
	
	
	public function save(Array $configuration)
	{
		$registry = bab_getRegistryInstance();
		$registry->changeDirectory('/requirecredentials/');
		
		$registry->setKeyValue('enabled', (bool) $configuration['enabled']);
		$registry->setKeyValue('exceptions', $configuration['exceptions']);
		$registry->setKeyValue('choose', (bool) $configuration['choose']);
		$registry->setKeyValue('timeout', (int) $configuration['timeout']);
		$registry->setKeyValue('modal', (bool) $configuration['modal']);
		$registry->setKeyValue('tophtml', $configuration['tophtml']);
		$registry->setKeyValue('bottomtext', $configuration['bottomtext']);
		
		bab_getBody()->addNextPageMessage(requirecredentials_translate('Options has been saved'));
		
		bab_url::get_request('tg')->location();
	}
}





if (!bab_isUserAdministrator())
{
	return;
}


/* @var $Icons Func_Icons */
$Icons = bab_functionality::get('Icons');
$Icons->includeCss();

$page = new requirecredentials_configEditor;

if (!empty($_POST))
{
	$page->save(bab_pp('configuration'));
}

$page->display();