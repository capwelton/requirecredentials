; <?php
// @codingStandardsIgnoreStart
/*
[general]
name                            ="requirecredentials"
version	                        ="0.4.3"
addon_type                      ="EXTENSION"
encoding                        ="UTF-8"
mysql_character_set_database    ="latin1,utf8"
description                     ="Require credentials on all pages with a configurable list of exceptions"
description.fr                  ="Module qui permet d'imposer un formulaire d'authentification pour toutes les pages"
delete                          =1
ov_version                      ="8.2.90"
php_version                     ="5.2.0"
author                          ="Cantico"
addon_access_control            ="0"
icon							="icon.png"
configuration_page              ="configuration"
tags                            ="extension,authentication,intranet"

[addons]
widgets                         =">=1.0.49"
LibTranslate				    =">=1.12.0rc3.01"


;*/
// @codingStandardsIgnoreStart
?>