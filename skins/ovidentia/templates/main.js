jQuery(document).ready(function() {
	
	var metadata = window.babAddonWidgets.getMetadata('requirecredentials-authtypes');
	
	jQuery('.requirecredentials-authtypes').dialog({
		modal:metadata.modal,
		width:'85%',
		dialogClass: "requirecredentials-dialog", // deprecated
		classes: {
			"ui-dialog": "requirecredentials-dialog"
		}
	});
	
	var timer = jQuery('.requirecredentials-authtypes .timer');
	var template = timer.text();
	var last = parseInt(template.match(/(\d+)s/)[1]);
	var timeoutID;
	
	function timerLoop() {
		last--;
		var newtext = template.replace(/\d+s/, last+'s');
		timer.text(newtext);
		
		if (0 === last) {
			document.location.href = jQuery('.requirecredentials-default').attr('href');
			return;
		}

		timeoutID = setTimeout(timerLoop, 1000);
	}
	
	timerLoop();
	
	jQuery('.requirecredentials-authtypes a').click(function() {
		if (timeoutID) {
			clearTimeout(timeoutID);
		}
	});
	
});

